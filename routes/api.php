<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('restaurant', 'RestaurantController');
    Route::apiResource('restaurant.review', 'RestaurantReviewController')->shallow();
    Route::apiResource('review.reply', 'ReviewReplyController')->shallow();
    Route::get('reviews', 'ReviewReplyController@reviewsIndex');
    Route::get('restaurant/{restaurant}/review/highest', 'RestaurantReviewController@highest');
    Route::get('restaurant/{restaurant}/review/lowest', 'RestaurantReviewController@lowest');
    Route::get('restaurant/{restaurant}/review/pending', 'RestaurantReviewController@pending');
    Route::get('profile/list', 'ProfileController@index');
    Route::get('profile/{user}', 'ProfileController@show');
    Route::get('profile/{user}/resturants', 'ProfileController@restaurants');
    Route::get('profile/{user}/reviews', 'ProfileController@reviews');
    Route::get('profile/{user}/reviews/pending', 'ProfileController@pendingReviews');
    Route::put('profile/{user}', 'ProfileController@update');
    Route::delete('profile/{user}', 'ProfileController@destroy');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('logout', 'AuthController@logout');
    Route::post('user', 'AuthController@user');
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('refresh', 'AuthController@refresh');
});
