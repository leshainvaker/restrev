<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\RestaurantStoreRequest;
use App\Http\Requests\RestaurantUpdateRequest;
use App\Models\Restaurant;
use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    const RESTAURANTS_PER_PAGE = 15;

    public function __construct()
    {
        $this->authorizeResource(Restaurant::class, 'restaurant');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        if (auth()->user()->isAdmin()) {
            return Restaurant::paginate(999999);
        }
        $query = Restaurant::select()->addSelect([
            'average' => Review::selectRaw('AVG(rate) as average')->whereColumn('restaurant_id', 'restaurants.id')->limit(1)
//            'average' => Review::selectRaw('CAST(AVG(rate) AS DECIMAL(10,2)) AS average')->whereColumn('restaurant_id', 'restaurants.id')->limit(1)
        ]);

        $filterData = json_decode($request->get('filterBy'), true);

        if (!empty($filterData['rate'])) {
            if ($filterData['rate'] == 'any') {
                $query->having('average', '>=', 0)
                    ->orHavingRaw('average is NULL');
            } else {
                $query->having('average', '>=', $filterData['rate']);
            }
        }

        if (auth()->user()->isOwner()) {
            $query->where('owner_id', auth()->id());
        }

        if (auth()->user()->isUser()) {
            $query->orderByDesc('average');
        }

        return $query->paginate($request->query('per_page', 15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RestaurantStoreRequest $request)
    {
        $data = $request->validated();
        if ($request->has('image')) {
            $image = $request->file('image');
            $path = $image->store('uploads');
            $data['image_path'] = $path;
        }
        $restaurant = auth()->user()->restaurants()->create($data);

        return $restaurant;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restaurant  $restaurant
     * @return Restaurant
     */
    public function show(Restaurant $restaurant)
    {
        return $restaurant->append(['highest_review', 'lowest_review']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function update(RestaurantUpdateRequest $request, Restaurant $restaurant)
    {
        $data = $request->all();
        if ($request->has('image')) {
            $image = $request->file('image');
            $path = $image->store('uploads');
            $data['image_path'] = $path;
        }

        $restaurant->update($data);

        return $restaurant;
    }

    /**
     * @param Restaurant $restaurant
     *
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(Restaurant $restaurant)
    {
        return $restaurant->delete();
    }

    /**
     * Get the map of resource methods to ability names.
     *
     * @return array
     */
    protected function resourceAbilityMap()
    {
        return [
            'index' => 'view',
            'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'update',
        ];
    }
}
