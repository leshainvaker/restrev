<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReplyUpdateRequest;
use App\Models\Review;
use App\Models\Reply;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewReplyController extends Controller
{

    public function index(Review $review)
    {
        return $review->reply;
    }

    public function reviewsIndex(Request $request)
    {
        if (auth()->user()->isAdmin()) {
            return Review::with('reply', 'restaurant')->paginate($request->query('per_page', 99999));
        }
        if (auth()->user()->isUser()) {
            return Review::with('reply', 'restaurant')->where('user_id', Auth::id())->paginate($request->query('per_page', 99999));
        }

        return Review::whereHas('restaurant', function (Builder $query) {
            $query->where('owner_id', Auth::id());
        })->paginate($request->query('per_page', 99999));
    }

    public function store(ReplyUpdateRequest $request, Review $review)
    {
        $this->authorize('create', [Reply::class, $review]);
        $data = $request->validated();
        $reply = new Reply($data);
        $reply->review()->associate($review);
        $reply->user()->associate(auth()->user());
        $reply->save();

        return $reply;
    }

    public function show(Reply $reply)
    {
        return $reply;
    }

    public function update(ReplyUpdateRequest $request, Reply $reply)
    {
        $this->authorize('update', [$reply]);

        $reply->update($request->validated());

        return $reply;
    }

    public function destroy(Reply $reply)
    {
        $this->authorize('update', [$reply]);

        return $reply->delete();
    }
}
