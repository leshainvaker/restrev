<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    const USERS_PER_PAGE = 15;
    const RESTAURANTS_PER_PAGE = 15;
    const REVIEWS_PER_PAGE = 15;

    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    public function index(Request $request)
    {
        return User::ofRole($request->query('role', null))->paginate($request->query('per_page', 99999));
    }

    public function show(User $user)
    {
        return $user;
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->validated());
        return $user;
    }

    public function restaurants(User $user)
    {
        return $user->restaurants()->paginate(self::RESTAURANTS_PER_PAGE);
    }

    public function reviews(User $user)
    {
        return $user->reviews()->paginate(self::REVIEWS_PER_PAGE);
    }

    public function pendingReviews(User $user)
    {
        return $user->restaurants()->reviews()->doesntHave('replies')->get();
    }

    public function destroy(User $user)
    {
        return $user->delete();
    }


    /**
     * Get the map of resource methods to ability names.
     *
     * @return array
     */
    protected function resourceAbilityMap()
    {
        return [
            'index' => 'viewAny',
            'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'delete',
            'restaurants' => 'show',
            'reviews' => 'show',
            'pendingReviews' => 'show',
        ];
    }
}
