<?php

namespace App\Http\Controllers\API;

use App\Models\Restaurant;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewUpdateRequest;
use App\Models\Review;

class RestaurantReviewController extends Controller
{
    const REVIEWS_PER_PAGE = 15;

    public function __construct()
    {
        $this->authorizeResource(Review::class, 'review');
    }

    public function index(Restaurant $restaurant)
    {
        return $restaurant->reviews()->paginate(self::REVIEWS_PER_PAGE);
    }

    public function lowest(Restaurant $restaurant)
    {
        return $restaurant->reviews()->orderBy('rate', 'asc')->first();
    }

    public function highest(Restaurant $restaurant)
    {
        return $restaurant->reviews()->orderBy('rate', 'desc')->first();
    }

    public function pending(Restaurant $restaurant)
    {
        return $restaurant->reviews()->doesntHave('replies');
    }

    public function store(ReviewUpdateRequest $request, Restaurant $restaurant)
    {
        $data = $request->validated();
        $review = new Review($data);
        $review->restaurant()->associate($restaurant);
        $review->user()->associate(auth()->user());
        $review->save();

        return $review;
    }

    public function show(Review $review)
    {
        return $review;
    }

    public function update(ReviewUpdateRequest $request, Review $review)
    {
        $review->update($request->validated());
        return $review;
    }

    public function destroy(Review $review)
    {
        return $review->delete();
    }

    /**
     * Get the map of resource methods to ability names.
     *
     * @return array
     */
    protected function resourceAbilityMap()
    {
        return [
            'index' => 'view',
            'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'destroy',
        ];
    }
}
