<?php

namespace App\Policies;

use App\Models\Reply;
use App\Models\Review;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReviewReplyPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Reply  $reply
     * @return mixed
     */
    public function view(User $user, Reply $reply)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, Review $review)
    {
        return $review->restaurant->owner->id == $user->id && $review->reply()->count() == 0;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Reply  $reply
     * @return mixed
     */
    public function update(User $user, Reply $reply)
    {
//        return $reply->user->id == $user->id;
        return $user->isAdmin();
    }

    public function delete(User $user, Reply $reply)
    {
        return $user->isAdmin();
    }
}
