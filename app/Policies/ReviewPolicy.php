<?php

namespace App\Policies;

use App\Models\Restaurant;
use App\Models\Review;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReviewPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function view()
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isOwner() ||  $user->isUser();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Review  $review
     * @return mixed
     */
    public function update(User $user, Review $review)
    {
        return $user->isAdmin();
    }

    public function destroy(User $user, Review $review)
    {
        return $user->isAdmin();
    }

    public function delete(User $user, Review $review)
    {
        return $user->isAdmin();
    }
}
