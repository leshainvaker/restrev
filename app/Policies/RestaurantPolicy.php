<?php

namespace App\Policies;

use App\Models\Restaurant;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RestaurantPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function create(User $user)
    {
        return $user->isOwner();
    }

    public function update(User $user, Restaurant $restaurant)
    {
//        return $user->isOwner() || $user->isAdmin();
        return $user->isAdmin();

    }

    public function view(User $user)
    {
        return true;
    }

    public function delete(User $user, Restaurant $restaurant)
    {
        return $user->isAdmin();
    }
}
