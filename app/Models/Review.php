<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Review extends Model
{
    protected $fillable = [
        'body', 'rate', 'visit_date'
    ];

    protected $visible = [
        'id', 'body', 'rate', 'visit_date', 'owner_reply', 'restaurant_id', 'author_name', 'restaurant_owner_id', 'user_id', 'restaurant', 'reply'
    ];

    protected $appends = [
        'owner_reply', 'author_name', 'restaurant_owner_id'
    ];

    public function reply()
    {
        return $this->hasOne(Reply::class, 'review_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function getOwnerReplyAttribute()
    {
        return $this->reply;
    }

    public function getAuthorNameAttribute()
    {
        return $this->user->first_name . " " . $this->user->last_name;
    }

    public function getRestaurantOwnerIdAttribute()
    {
        return $this->restaurant->owner_id;
    }
}
