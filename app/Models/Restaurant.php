<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

use JustBetter\PaginationWithHavings\PaginationWithHavings;

/**
 * Class Restaurant
 *
 * @property User $owner
 * @package App\Models
 */
class Restaurant extends Model
{
    use PaginationWithHavings;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'image_path'];

    protected $visible = [
        'id', 'owner_id', 'name', 'description', 'image_url', 'average', 'reviews_count', 'lowest_review', 'highest_review', 'owner'
    ];

    protected $appends = ['image_url', 'average', 'reviews_count'];

    protected $with = ['owner'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'restaurant_id');
    }

    public function getImageUrlAttribute()
    {
        return asset($this->image_path);
    }

    public function getReviewsCountAttribute()
    {
        return $this->reviews()->count();
    }

    public function getAverageAttribute()
    {
        return round($this->reviews()->avg('rate'), 1);
    }

    public function getLowestReviewAttribute()
    {
        return $this->reviews()->orderBy('rate', 'asc')->first();
    }

    public function getHighestReviewAttribute()
    {
        return $this->reviews()->orderBy('rate', 'desc')->first();
    }
}
