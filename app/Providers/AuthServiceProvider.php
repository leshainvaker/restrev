<?php

namespace App\Providers;

use App\Models\Restaurant;
use App\Models\Review;
use App\Models\Reply;
use App\Policies\ProfilePolicy;
use App\Policies\RestaurantPolicy;
use App\Policies\ReviewPolicy;
use App\Policies\ReviewReplyPolicy;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Restaurant::class => RestaurantPolicy::class,
        Review::class => ReviewPolicy::class,
        Reply::class => ReviewReplyPolicy::class,
        User::class => ProfilePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
