<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    public function testUserLogout()
    {
        // Simulating login
        $user = factory(User::class)->create();

        $this
            ->actingAs($user)
            ->json('get', '/api/auth/logout', [])
            ->assertStatus(200);
    }
}
