<?php

namespace Tests\Feature;

use App\Models\Restaurant;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RestaurantTest extends TestCase
{
    public function testsRestaurantAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();

        $payload = [
            'name' => 'Name',
            'description' => 'Description',
        ];

        $this
            ->actingAs($user)
            ->json('POST', '/api/restaurant', $payload)
            ->assertStatus(201)
            ->assertJson(['id' => 1, 'name' => 'Name', 'description' => 'Description']);
    }

    public function testsRestaurantAreCreatedByRegularUser()
    {
        $user = factory(User::class)->create([
            'email' => 'testlogin@user.com',
            'password' => bcrypt('password'),
            'role' => 'user'
        ]);

        $payload = [
            'name' => 'Name',
            'description' => 'Description',
        ];

        $this
            ->actingAs($user)
            ->json('POST', '/api/restaurant', $payload)
            ->assertStatus(403);
    }

    public function testsRestaurantAreUpdatedCorrectly()
    {
        $user = factory(User::class)->create();

        $rest = factory(Restaurant::class)->create();

        $payload = [
            'name' => 'Name',
            'description' => 'Description'
        ];

        $this
            ->actingAs($user)
            ->json('PUT', '/api/restaurant/' . $rest->id, $payload)
            ->assertStatus(200)
            ->assertJson([
                'id' => $rest->id,
                'name' => 'Name',
                'description' => 'Description'
            ]);
    }

    public function testsRestaurantAreUpdatedByRegularUser()
    {
        $user = factory(User::class)->create([
            'email' => 'testlogin@user.com',
            'password' => bcrypt('password'),
            'role' => 'user'
        ]);

        $rest = factory(Restaurant::class)->create();

        $payload = [
            'name' => 'Name',
            'description' => 'Description'
        ];

        $this
            ->actingAs($user)
            ->json('PUT', '/api/restaurant/' . $rest->id, $payload)
            ->assertStatus(403);
    }

    public function testsRestaurantAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();

        $rest = factory(Restaurant::class)->create();

        $this
            ->actingAs($user)
            ->json('DELETE', '/api/restaurant/' . $rest->id, [])
            ->assertStatus(200);
    }

    public function testsRestaurantAreDeletedByRegularUser()
    {
        $user = factory(User::class)->create([
            'email' => 'testlogin@user.com',
            'password' => bcrypt('password'),
            'role' => 'user'
        ]);

        $rest = factory(Restaurant::class)->create();

        $this
            ->actingAs($user)
            ->json('DELETE', '/api/restaurant/' . $rest->id, [])
            ->assertStatus(403);
    }

    public function testArticlesAreListedCorrectly()
    {
        $user = factory(User::class)->create();

        factory(Restaurant::class, 5)->create();

        $this
            ->actingAs($user)
            ->json('GET', '/api/restaurant', [])
            ->assertStatus(200)
            ->assertJsonStructure([
                'current_page',
                'data'
            ]);
    }

    public function testsRestaurantNameAndDescriptionRequire()
    {
        $user = factory(User::class)->create();

        $this
            ->actingAs($user)
            ->json('post', '/api/restaurant')
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'name' => ['The name field is required.'],
                    'description' => ['The description field is required.'],
                ]
            ]);
    }
}
