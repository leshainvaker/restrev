<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function testsRegistersSuccessfully()
    {
        $payload = [
            'first_name' => 'John',
            'last_name' => 'Bond',
            'birthday_date' => '2000-12-25',
            'password' => 'password',
            'email' => 'email@test.com',
            'isOwner' => '1',
        ];

        $this->json('post', '/api/auth/register', $payload)
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Successfully registration!'
            ]);
    }

    public function testsRequiresNameAndBirthdayAndPasswordAndEmailAndIsOwner()
    {
        $this->json('post', '/api/auth/register')
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'first_name' => ['The first name field is required.'],
                    'last_name' => ['The last name field is required.'],
                    'birthday_date' => ['The birthday date field is required.'],
                    'password' => ['The password field is required.'],
                    'email' => ['The email field is required.'],
                    'isOwner' => ['The is owner field is required.'],
                ]
            ]);
    }
}
