<?php

namespace Tests\Feature;

use App\Models\Restaurant;
use App\Models\Review;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReviewTest extends TestCase
{
    public function testsReviewAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();

        $payload = [
            'name' => 'Name',
            'description' => 'Description',
        ];

        $this
            ->actingAs($user)
            ->json('POST', '/api/restaurant', $payload)
            ->assertStatus(201)
            ->assertJson(['id' => 1, 'name' => 'Name', 'description' => 'Description']);
    }

    public function testsReviewAreUpdatedCorrectly()
    {
        $user = factory(User::class)->create([
            'email' => 'testlogin@user.com',
            'password' => bcrypt('password'),
            'role' => 'admin'
        ]);

        $rest = factory(Restaurant::class)->create();
        $review = factory(Review::class)->create([
            'user_id' => $user->id,
            'rate' => 5,
            'visit_date' => '2000-12-23',
        ]);

        $payload = [
            'rate' => 4,
            'visit_date' => '2000-12-25'
        ];

        $this
            ->actingAs($user)
            ->json('PUT', '/api/review/' . $review->id , $payload)
            ->assertStatus(200)
            ->assertJson([
                'rate' => '4',
                'visit_date' => '2000-12-25'
            ]);
    }

    public function testsReviewAreUpdatedByOwner()
    {
        $user = factory(User::class)->create([
            'email' => 'testlogin@user.com',
            'password' => bcrypt('password'),
            'role' => 'owner'
        ]);

        $rest = factory(Restaurant::class)->create();
        $review = factory(Review::class)->create();

        $payload = [
            'rate' => 4,
            'visit_date' => '2000-12-25'
        ];

        $this
            ->actingAs($user)
            ->json('PUT', '/api/restaurant/' . $review->id . '/review', $payload)
            ->assertStatus(405);
    }


    public function testsReviewAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();

        $rest = factory(Restaurant::class)->create();
        $review = factory(Review::class)->create();

        $this
            ->actingAs($user)
            ->json('DELETE', '/api/review/' . $review->id, [])
            ->assertStatus(200);
    }


    public function testReviewAreListedCorrectly()
    {
        $user = factory(User::class)->create();

        factory(Restaurant::class, 5)->create();
        factory(Review::class, 5)->create();


        $this
            ->actingAs($user)
            ->json('GET', '/api/reviews', [])
            ->assertStatus(200)
            ->assertJsonStructure([
                'current_page',
                'data'
            ]);
    }

    public function testsReviewRateAndVisitDateRequire()
    {
        $user = factory(User::class)->create();
        $rest = factory(Restaurant::class)->create();

        $this
            ->actingAs($user)
            ->json('post', '/api/restaurant/'.$rest->id.'/review')
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'rate' => ['The rate field is required.'],
                    'visit_date' => ['The visit date field is required.'],
                ]
            ]);
    }
}
