<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    public function testsProfileAreUpdatedCorrectly()
    {
        $user = factory(User::class)->create();

        $payload = [
            'first_name' => 'Newname'
        ];

        $this
            ->actingAs($user)
            ->json('PUT', '/api/profile/' . $user->id, $payload)
            ->assertStatus(200)
            ->assertJson([
                'id' => $user->id,
                'first_name' => 'Newname'
            ]);
    }

    public function testsProfileAreDeletedCorrectly()
    {
        $user = factory(User::class)->create([
            'email' => 'testlogin@user.com',
            'password' => bcrypt('password'),
            'role' => 'admin'
        ]);
        $user2 = factory(User::class)->create();

        $this
            ->actingAs($user)
            ->json('DELETE', '/api/profile/' . $user2->id)
            ->assertStatus(200);
    }

    public function testProfilesAreListedCorrectly()
    {
        $user = factory(User::class)->create();

        $this
            ->actingAs($user)
            ->json('GET', '/api/profile/list', [])
            ->assertStatus(200)
            ->assertJsonStructure([
                'current_page',
                'data'
            ]);
    }
}
