<?php

namespace Tests\Feature;

use App\Models\Reply;
use App\Models\Restaurant;
use App\Models\Review;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReplyTest extends TestCase
{
    public function testsReplyAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $review = factory(Review::class)->create();

        $payload = [
            'body' => 'Body'
        ];

        $this
            ->actingAs($user)
            ->json('POST', '/api/review/'.$review->id.'/reply', $payload)
            ->assertStatus(201)
            ->assertJson(['body' => 'Body']);
    }

    public function testReplyAreListedCorrectly()
    {
        $user = factory(User::class)->create();

        $review = factory(Review::class)->create();
        factory(Reply::class, 5)->create();

        $this
            ->actingAs($user)
            ->json('GET', '/api/review/'.$review->id.'/reply', [])
            ->assertStatus(200);
    }

    public function testsReplyBodyRequire()
    {
        $user = factory(User::class)->create();
        $review = factory(Review::class)->create();

        $this
            ->actingAs($user)
            ->json('post', '/api/review/'.$review->id.'/reply')
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'body' => ['The body field is required.'],
                ]
            ]);
    }
}
