#RestView

##Installation

Run:
- `git clone -b dev https://stanislavqq@bitbucket.org/pandalaravel/revrest.git`
- copy revrest folder to the server files folder
- configure nginx or vagrant
- `cd {application_folder}`
- `configure .env` - configure DB_HOST DB_DATABASE DB_USERNAME DB_PASSWORD
- `composer install`
- `php artisan key:generate`
- `php artisan jwt:secret`
- `php artisan migrate`
- `npm install`
- `npm run dev`
- api points available on http://{local_address_of_app}/api/
- web points available on http://{local_address_of_app}/


##Endpoints docs

Coming soon