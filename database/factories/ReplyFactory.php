<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Reply;
use \App\Models\Review;
use Faker\Generator as Faker;


$factory->define(Reply::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'review_id' => function () {
            return factory(Review::class)->create()->id;
        },
        'body' => $faker->paragraph,
        'created_at' => $faker->date()
    ];
});
