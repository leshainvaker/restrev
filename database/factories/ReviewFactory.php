<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;

$factory->define(\App\Models\Review::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'restaurant_id' => function () {
            return factory(\App\Models\Restaurant::class)->create()->id;
        },
        'body' => $faker->text(),
        'rate' => $faker->randomElement([1, 2, 3, 4, 5]),
        'visit_date' => $faker->date(),
        'created_at' => $faker->date()
    ];
});
