<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create admin user
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'role' => 'admin',
            'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
            'birthday_date' => '1991-06-01',
            'email' => 'admin@admin.com',
        ]);
    }
}
