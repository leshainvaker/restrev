require("./bootstrap");

window.Vue = require("vue");
import vuetify from "../plugins/vuetify";
import Vuex from "vuex";
import router from "./router";
import App from "./views/App";
import store from "./store";
import configureInterceptors from "./helpers/api";
import { extend } from "vee-validate";
import { required, email, max, min } from "vee-validate/dist/rules";
import VueConfirmDialog from "vue-confirm-dialog";
import FlashMessage from '@smartweb/vue-flash-message';


configureInterceptors();


extend("required", {
  ...required,
  message: "{_field_} can not be empty"
});

extend("max", {
  ...max,
  message: "{_field_} may not be greater than {length} characters"
});

extend("min", {
  ...min,
  message: "{_field_} may not have less than {length} letters"
});

extend("email", {
  ...email,
  message: "Email must be valid"
});


extend("password", {
  params: ["target"],
  validate(value, { target }) {
      return value === target;
  },
  message: "Password confirmation does not match"
});

Vue.use(VueConfirmDialog);
Vue.use(FlashMessage);

Vue.use(Vuex);
const app = new Vue({
    vuetify,
    router,
    store,
    el: "#app",
    render: h => h(App)
});

export default app;
