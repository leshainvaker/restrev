import Vue from "vue";
import Vuex from "vuex";
import auth from "./modules/auth";
import user from "./modules/user";
import restaurants from "./modules/restaurants";
import reviews from "./modules/reviews";
Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        auth,
        user,
        restaurants,
        reviews
    }
});

export default store;
