export const USER_SIGNUP = "USER_SIGNUP";
export const USER_REQUEST = "USER_REQUEST";
export const GET_USERS_LIST = 'GET_USERS_LIST';
export const CREATE_USER = 'CREATE_USER';
export const GET_USER = 'GET_USER';
export const EDIT_USER = "EDIT_USER";
export const DELETE_USER = "DELETE_USER";
