export const CREATE_REPLY = "CREATE_REPLY"
export const EDIT_REPLY = "EDIT_REPLY"
export const EDIT_REVIEW = "EDIT_REVIEW"
export const CREATE_REVIEW = "CREATE_REVIEW"
export const DELETE_REVIEW = "DELETE_REVIEW"
export const DELETE_REPLY = "DELETE_REPLY"
export const GET_REPLIES = "GET_REPLIES"
export const GET_REVIEWS = "GET_REVIEWS"
export const UPDATE_REVIEWS = "UPDATE_REVIEWS"
