import Vue from "vue";
import {
    GET_RESTAURANTS_LIST,
    GET_RESTAURANT,
    GET_RESTAURANT_REVIEWS, CREATE_RESTAURANT,
    EDIT_RESTAURANT, DELETE_RESTAURANT, UPDATE_RESTAURANTS, GET_USER_RESTAURANTS_LIST
} from "../actions/restaurants";
import axios from "axios";
import {CREATE_REVIEW} from "../actions/reviews";
import {DELETE_USER, EDIT_USER} from "../actions/user";

const state = {
    restaurants: {},
};

const getters = {
    [GET_RESTAURANTS_LIST]: state => {
        return state.restaurants;
    },
};

const mutations = {
    [DELETE_RESTAURANT]: (state, data) => {
        let arr = state.restaurants.data
        const index = arr.indexOf(data)
        arr.splice(index, 1)
        let res = state.restaurants
        res.data = arr

        Vue.set(state, "restaurants", res)
    },
    [GET_RESTAURANTS_LIST]: (state, data) => {
        Vue.set(state, "restaurants", data);
    },
    [UPDATE_RESTAURANTS]: (state, data) => {
        let arr = state.restaurants.data
        let removeIndex = arr.map(function (item) {
            return item.id
        }).indexOf(data.data.id)
        arr.splice(removeIndex, 1)
        arr.push(data.data)
        arr.sort(function (a, b) {
            if (a.id < b.id) {
                return -1
            }
            if (a.id > b.id) {
                return 1
            }
            return 0
        })
        let res = state.restaurants
        res.data = arr
        Vue.set(state, "restaurants", res)
    }
}

const actions = {
    [GET_RESTAURANTS_LIST]: ({ commit }, { page, per_page, filterBy }) => {
        if (page === undefined) {
            page = 1;
        }
        if (per_page === undefined) {
            per_page = 5;
        }

        return new Promise((resolve, reject) => {
            axios.get("/restaurant", { params: { page, per_page, filterBy } }).then(response => {
                resolve(response.data);
                commit(GET_RESTAURANTS_LIST, response.data);
            });
        });
    },
    [CREATE_RESTAURANT]: (context, restaurantData) => {
        return axios.post("/restaurant", restaurantData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
    },
    [GET_RESTAURANT]: (context, id) => {
        return axios.get("/restaurant/" + id);
    },
    [GET_RESTAURANT_REVIEWS]: (context, id) => {
        return axios.get("/restaurant/" + id + "/review");
    },
    [EDIT_RESTAURANT]: ({ commit }, restaurantData) => {
        return new Promise((resolve, reject) => {
            axios.post("/restaurant/" + restaurantData.get('id'), restaurantData).then(response => {
                resolve(response);
                commit(UPDATE_RESTAURANTS, response);
            });
        });
    },
    [DELETE_RESTAURANT]: ({ commit }, restaurantData) => {
        return new Promise((resolve, reject) => {
            axios.delete("/restaurant/" + restaurantData.id).then(response => {
                commit(DELETE_RESTAURANT, restaurantData);
            });
        });
    },
};

export default {
    state,
    getters,
    mutations,
    actions
};
