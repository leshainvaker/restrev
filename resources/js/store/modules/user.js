import axios from "axios";
import Vue from "vue";
import {USER_SIGNUP, USER_REQUEST, GET_USERS_LIST, GET_USER, CREATE_USER, EDIT_USER, DELETE_USER} from "../actions/user";
import {DELETE_REVIEW, EDIT_REVIEW} from "../actions/reviews";
import {DELETE_RESTAURANT} from "../actions/restaurants";

const state = {
    user: {},
    users: {}
};

const getters = {
    [GET_USERS_LIST]: state => {
        return state.users;
    }
};

const actions = {
    [USER_SIGNUP]: (context, user) => {
        return new Promise((resolve, reject) => {
            axios
                .post("auth/register", user)
                .then(response => {
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },
    [USER_REQUEST]: ({commit}) => {
        return new Promise((resolve, reject) => {
            axios
                .get("auth/user")
                .then(response => {
                    commit(USER_REQUEST, response.data);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },
    [GET_USERS_LIST]: ({ commit }, page) => {
        if (page === undefined) {
            page = 1;
        }

        return new Promise((resolve, reject) => {
            axios.get("/profile/list", { params: page }).then(response => {
                resolve(response.data);
                commit(GET_USERS_LIST, response.data);
            });
        });
    },
    [GET_USER]: (context, id) => {
        return axios.get("/profile/" + id);
    },
    [EDIT_USER]: (context, userData) => {
        return axios.put("/profile/" + userData.id, userData);
    },
    [DELETE_USER]: ({ commit }, userData) => {
        return new Promise((resolve, reject) => {
            axios.delete("/profile/" + userData.id).then(response => {
                commit(DELETE_USER, userData);
            });
        });
    },
};

const mutations = {
    [USER_REQUEST]: (state, user) => {
        Vue.set(state, "user", user);
    },
    [GET_USERS_LIST]: (state, data) => {
        Vue.set(state, "users", data);
    },
    [DELETE_USER]: (state, data) => {
        let arr = state.users.data
        const index = arr.indexOf(data)
        arr.splice(index, 1)
        let res = state.users
        res.data = arr

        Vue.set(state, "users", res)
    },
};
export default {
    state,
    getters,
    actions,
    mutations
};
