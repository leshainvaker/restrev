import Vue from "vue";
import { CREATE_REPLY, EDIT_REPLY, EDIT_REVIEW, CREATE_REVIEW, DELETE_REPLY, DELETE_REVIEW, UPDATE_REVIEWS, GET_REVIEWS } from "../actions/reviews";
import axios from "axios";
import {DELETE_RESTAURANT, GET_RESTAURANTS_LIST, UPDATE_RESTAURANTS} from "../actions/restaurants";
const state = {
    reviews: {}
};
const getters = {
    [GET_REVIEWS]: state => {
        return state.reviews;
    },
};
const actions = {
    [CREATE_REPLY]: (context, { reviewId, replyData }) => {
        return axios.post("/review/" + reviewId + "/reply", replyData);
    },
    [EDIT_REPLY]: (context, replyData) => {
        return axios.put("/reply/" + replyData.id, replyData);
    },
    [EDIT_REPLY]: (context, replyData) => {
        return axios.put("/reply/" + replyData.id, replyData);
    },
    [CREATE_REVIEW]: (context, {restaurantId, reviewData}) => {
        return axios.post("/restaurant/" + restaurantId + "/review", reviewData);
    },
    [EDIT_REVIEW]: ({ commit }, reviewData) => {
        return new Promise((resolve, reject) => {
            axios.put("/review/" + reviewData.id, reviewData).then(response => {
                resolve(response);
                commit(UPDATE_REVIEWS, response);
            });
        });
    },
    [DELETE_REVIEW]: ({ commit }, reviewData) => {
        return new Promise((resolve, reject) => {
            axios.delete("/review/" + reviewData.id).then(response => {
                commit(DELETE_REVIEW, reviewData);
            });
        });
    },
    [DELETE_REPLY]: (context, replyId) => {
        return axios.delete("/reply/" + replyId);
    },
    [GET_REVIEWS]: ({ commit }, page) => {
        if (page === undefined) {
            page = 1;
        }

        return new Promise((resolve, reject) => {
            axios.get("/reviews", { params: { page } }).then(response => {
                resolve(response.data);
                commit(GET_REVIEWS, response.data);
            });
        });
    },
};
const mutations = {
    [DELETE_REVIEW]: (state, data) => {
        let arr = state.reviews.data
        const index = arr.indexOf(data)
        arr.splice(index, 1)
        let res = state.reviews
        res.data = arr

        Vue.set(state, "reviews", res)
    },
    [GET_REVIEWS]: (state, data) => {
        Vue.set(state, "reviews", data);
    },
    [UPDATE_REVIEWS]: (state, data) => {
        let arr = state.reviews.data
        let removeIndex = arr.map(function (item) {
            return item.id
        }).indexOf(data.data.id)
        arr.splice(removeIndex, 1)
        arr.push(data.data)
        arr.sort(function (a, b) {
            if (a.id < b.id) {
                return -1
            }
            if (a.id > b.id) {
                return 1
            }
            return 0
        })
        let res = state.reviews
        res.data = arr
        Vue.set(state, "reviews", res)
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
