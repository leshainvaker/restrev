import {
    AUTH_REQUEST,
    AUTH_LOGOUT,
    AUTH_SUCCESS,
    AUTH_REFRESH
} from "../actions/auth";
import axios from "axios";
import jwt from "jsonwebtoken";

const state = {
    access_token: localStorage.getItem("access-token") || "",
    role: localStorage.getItem("role") || "",
    user_id: localStorage.getItem("user_id") || ""
};

const getters = {
    isAuthenticated: state => !!state.access_token,
    getUserRole: state => state.role,
    getUserId: state => state.user_id,
    isOwner: state => state.role === "owner",
    isUser: state => state.role === "user",
    isAdmin: state => state.role === "admin"
};

const actions = {
    [AUTH_REQUEST]: ({ commit }, user) => {
        return new Promise((resolve, reject) => {
            axios
                .post("auth/login", user)
                .then(response => {
                    commit(AUTH_SUCCESS, response);
                    resolve(response);
                })
                .catch(error => {
                    commit(AUTH_LOGOUT);
                    reject(error);
                });
        });
    },
    [AUTH_REFRESH]: ({ commit }) => {
        return new Promise((resolve, reject) => {
            axios
                .post("auth/refresh")
                .then(response => {
                    commit(AUTH_SUCCESS, response);
                    resolve(response);
                })
                .catch(error => {
                    commit(AUTH_LOGOUT);
                    reject(error);
                });
        });
    },
    [AUTH_LOGOUT]: ({ commit }) => {
        return new Promise((resolve, reject) => {
            axios
                .post("auth/logout")
                .then(response => {
                    commit(AUTH_LOGOUT);
                    resolve(response);
                })
                .catch(error => {
                    commit(AUTH_LOGOUT);
                    reject(error);
                });
        });
    }
};

const mutations = {
    [AUTH_SUCCESS]: (state, resp) => {
        const { access_token } = resp.data;
        let { role, user_id } = jwt.decode(access_token);
        Vue.set(state, "access_token", access_token);
        Vue.set(state, "role", role);
        Vue.set(state, "user_id", user_id);
        localStorage.setItem("access-token", access_token);
        localStorage.setItem("role", role);
        localStorage.setItem("user_id", user_id);
    },
    [AUTH_LOGOUT]: state => {
        Vue.set(state, "access_token", "");
        Vue.set(state, "role", "");
        Vue.set(state, "user_id", "");
        localStorage.removeItem("access-token");
        localStorage.removeItem("role");
        localStorage.removeItem("user_id");
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
