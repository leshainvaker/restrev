import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";
import Home from "../pages/Home";
import SignUp from "../pages/SignUp";
import SignIn from "../pages/SignIn";
import Restaurants from "../pages/restaurants";
import RestaurantsCreate from "../pages/restaurantsCreate";
import Restaurant from "../pages/restaurant";
import User from "../pages/users"
import AdminReviews from "../components/admin/AdminReviews";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },
        {
            path: "/sign-up",
            name: "sign-up",
            component: SignUp,
            meta: {
                guest: true
            }
        },
        {
            path: "/sign-in",
            name: "sign-in",
            component: SignIn,
            meta: {
                guest: true
            }
        },
        {
            path: "/restaurants",
            name: "restaurants",
            component: Restaurants,
            meta: {
                auth: true
            }
        },
        {
            path: "/restaurants/create",
            name: "restaurants-create",
            component: RestaurantsCreate,
            meta: {
                auth: true
            }
        },
        {
            path: "/restaurant/:restaurant_id",
            name: "restaurant-page",
            component: Restaurant,
            meta: {
                auth: true
            }
        },
        {
            path: "/profile/list",
            name: "users",
            component: User,
            meta: {
                auth: true
            }
        },
        {
            path: "/reviews",
            name: "reviews",
            component: AdminReviews,
            meta: {
                auth: true
            }
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (router.options.routes.find(x => x.name === to.name) === undefined || to.name === 'home') {
        if (!store.getters.isAuthenticated) {
            next("/sign-in");
            return;
        }
        if (store.getters.isAuthenticated) {
            next("/restaurants");
            return;
        }
    }

    if (to.matched.some(record => record.meta.auth)) {
        if (!store.getters.isAuthenticated) {
            next("/sign-in");
            return;
        }
    } else if (to.matched.some(record => record.meta.guest)) {
        if (store.getters.isAuthenticated) {
            next("/");
            return;
        }
    }

    next();
});

export default router;
