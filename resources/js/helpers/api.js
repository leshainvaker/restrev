import axios from "axios";
import store from "../store";
import { AUTH_REFRESH, AUTH_SUCCESS, AUTH_LOGOUT } from "../store/actions/auth";

axios.defaults.baseURL = "http://127.0.0.1:8000/api/";
// axios.defaults.baseURL = "https://panda.test/api/";
export default function configureInterceptors() {
    axios.interceptors.request.use(
        config => {
            let token = localStorage.getItem("access-token");
            if (token) {
                config.headers["Authorization"] = `Bearer ${token}`;
            }
            return config;
        },
        error => {
            return Promise.reject(error);
        }
    );

    axios.interceptors.response.use(
        response => {
            return response;
        },
        error => {
            if (
                error.response.status === 401 &&
                store.getters.isAuthenticated
            ) {
                axios
                    .post("auth/refresh")
                    .then(response => {
                        store.commit(AUTH_SUCCESS, response);
                    })
                    .catch(error => {
                        if (error.status === 401) {
                            store.commit(AUTH_LOGOUT);
                        }
                    });
            }
            return Promise.reject(error);
        }
    );
}
